package main.java.problems;

import main.java.adt.linkedList.SingleLinkedListNode;
import main.java.adt.queue.QueueOverflowException;
import main.java.adt.queue.QueueUnderflowException;

public class QueueLinkedListNodeImpl<T> implements QueueLinkedListNode<T> {

    private SingleLinkedListNode<T> head;
    private SingleLinkedListNode<T> tail;
    private int tamanho;
    private int capacidade;

    /**
     * Construtor da classe. Esse construtor precisa existir.
     * Restricao:
     * - NÃO altere a assinatura desse construtor. 
     *   Voce pode ajustar apenas o corpo do construtor, se necessário.
     */
    public QueueLinkedListNodeImpl(int size) {
        head = new SingleLinkedListNode<>();
        tail = new SingleLinkedListNode<>();
        tamanho = 0;
        capacidade = size;
    }

    public void enqueue(T element) throws QueueOverflowException {
        if (isFull()) {
            throw new QueueOverflowException();
        }

        if (element != null) {
            SingleLinkedListNode<T> newNode = new SingleLinkedListNode<>(element, new SingleLinkedListNode<>());
            if (isEmpty()) {
                head = newNode;
            } else {
                tail.setNext(newNode);
            }
            tail = newNode;
            tamanho++;
        }
    }

    public T dequeue() throws QueueUnderflowException {
        if (isEmpty()) {
            throw new QueueUnderflowException();
        }

        T element = head.getData();
        head = head.getNext();
        tamanho--;
        if (isEmpty()) {
            tail = new SingleLinkedListNode<>();
        }
        return element;
    }

    public T head() {
        return head.getData();
    }

    public boolean isEmpty() {
        return tamanho == 0;
    }

    public boolean isFull() {
        return tamanho == capacidade;
    }

    public int size() {
        return tamanho;
    }

}
